from discord.ext import commands
from handlers.brief import handle_brief
from handlers.envs import handle_envs
from handlers.helpers import get_projects_names, get_elements_names
from handlers.sync import handle_sync
import discord
import logging
import yaml

logging.basicConfig(level=logging.INFO)


def load_config(config_file: str):
    with open(config_file, "r") as f:
        return yaml.load(f, Loader=yaml.FullLoader)


config = load_config('config.yml')
bot = discord.Bot()
loop = bot.loop

projects_names = get_projects_names(config)
elements_names = get_elements_names(config)


@bot.command(description='List elements for single project')
async def envs(interaction: discord.Interaction,
               project: discord.Option(str, choices=projects_names, description='Проект', required=True)):
    await interaction.response.defer(ephemeral=True)
    embeds = await loop.run_in_executor(
        None, handle_envs, config, project)
    await interaction.followup.send(embeds=embeds, ephemeral=True)


@bot.command(description='List of main projects in all elements')
async def brief(interaction: discord.Interaction,
                element: discord.Option(str, choices=elements_names, description='Стихия', required=True)):
    await interaction.response.defer(ephemeral=True)
    embeds = await loop.run_in_executor(
        None, handle_brief, config, element)
    await interaction.followup.send(embeds=embeds, ephemeral=True)


@bot.command(description='Deploy any project branch to element')
async def sync(interaction: discord.Interaction,
               project: discord.Option(str, choices=projects_names, description='Проект', required=True),
               element: discord.Option(str, choices=elements_names, description='Стихия', required=True),
               branch:  discord.Option(str, description='Ветка', required=True)):
    await interaction.response.defer()
    response = await loop.run_in_executor(
        None, handle_sync, config, project, element, branch)
    interaction.followup.send(response)


@sync.error
async def sync_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.respond("You don't have the correct role or sent a private message.")


@envs.error
async def envs_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.respond("You don't have the correct role or sent a private message.")


@brief.error
async def brief_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.respond("You don't have the correct role or sent a private message.")


bot.run(config['discord']['token'])
