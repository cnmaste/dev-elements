from .helpers import find_project_by_name
from gitlab.exceptions import GitlabCreateError
import gitlab


def handle_sync(config: dict, project_name: str, element_name: str, branch_name: str) -> str:
    gl = gitlab.Gitlab(url=config['gitlab']['apiUrl'], private_token=config['gitlab']['token'])
    project_cfg = find_project_by_name(project_name, config['projects'])
    if project_cfg:
        project_id = int(project_cfg['id'])
        project = gl.projects.get(project_id)
        token = project_cfg['token']
        try:
            pipeline = project.trigger_pipeline(branch_name, token, variables={'ELEMENT_TARGET': element_name})
        except GitlabCreateError:
            return 'Error: can\'t create for some reason..'

        status = pipeline.attributes['status']
        return f'Command sent with status: {status}'
    else:
        return 'Project not found'
