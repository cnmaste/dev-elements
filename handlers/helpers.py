import dateutil.parser
import discord
import logging
import re


DISCORD_COLOR_RED = '0xEE6C4D'
DISCORD_COLOR_YELLOW = '0xFFE66D'
DISCORD_COLOR_BLUE = '0x98C1D9'
DISCORD_COLOR_WHITE = '0x3D5A80'


def parse_message(a):
    # TODO: optional url link replace
    return a


def pick_color_from_state(state):
    colors = {
        'opened': DISCORD_COLOR_RED,
        'closed': DISCORD_COLOR_YELLOW,
        'merged': DISCORD_COLOR_BLUE
    }
    return int(colors.get(state, DISCORD_COLOR_WHITE), 16)


def find_project_by_name(project_name: str, projects: list) -> dict:
    current_project = None
    try:
        current_project = next(project for project in projects if project["name"] == project_name)
    except StopIteration:
        logging.error(f'Error finding project {project_name}')
    return current_project


def get_projects_names(config: dict) -> list:
    return [project['name'] for project in config['projects']]


def get_elements_names(config: dict) -> list:
    return list(config['elements'].keys())


def generate_envs_embeds(project, environments_cfg) -> list[discord.Embed]:
    embeds = []
    project_name = project.attributes['name']
    for env_name, env_id in environments_cfg.items():
        environment = project.environments.get(env_id)
        try:
            deployment_ref = environment.attributes["last_deployment"]["ref"]
        except TypeError:
            continue
        m = re.match("refs/merge-requests/(\d+)/head", deployment_ref)
        updated_at = dateutil.parser.isoparse(environment.attributes["last_deployment"]["updated_at"]).strftime("%d %b")
        if m:
            mr_iid = m.group(1)
            mr = project.mergerequests.get(mr_iid)
            title = mr.attributes["title"]
            state = mr.attributes["state"]
            author = mr.attributes["author"]["username"]
        else:
            title = environment.attributes["last_deployment"]["deployable"]["commit"]["title"]
            state = "manual"
            author = environment.attributes["last_deployment"]["deployable"]["commit"]["author_name"]
        color = pick_color_from_state(state)
        embed = discord.Embed(title=f'{project_name} {env_name}', description=parse_message(title), color=color)
        embed.add_field(name='Updated', value=updated_at, inline=True)
        embed.add_field(name='State', value=state, inline=True)
        embed.add_field(name='Author', value=author, inline=True)
        embeds.append(embed)
    return embeds


def light_projects_only(project):
    return project['list'] == 'light'
