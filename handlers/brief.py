from .helpers import generate_envs_embeds, light_projects_only
import discord
import gitlab
import logging


def handle_brief(config: dict,  element_name: str) -> list[discord.Embed]:
    embeds = []
    gl = gitlab.Gitlab(url=config['gitlab']['apiUrl'], private_token=config['gitlab']['token'])
    light_projects = filter(light_projects_only, config['projects'])
    for project_cfg in light_projects:
        project_id = project_cfg['id']
        environments_cfg = project_cfg['envs']
        filtered_environments_cfg = {env_name: env_id for env_name, env_id in environments_cfg.items()
                                     if env_name == element_name}
        project = gl.projects.get(project_id)
        project_embeds = generate_envs_embeds(project, filtered_environments_cfg)
        embeds.extend(project_embeds)
    return embeds
