from .helpers import find_project_by_name, generate_envs_embeds
from gitlab.exceptions import GitlabGetError
import discord
import gitlab
import logging


def handle_envs(config: dict,  project_name: str) -> list[discord.Embed]:
    embeds = []
    gl = gitlab.Gitlab(url=config['gitlab']['apiUrl'], private_token=config['gitlab']['token'])
    project_cfg = find_project_by_name(project_name, config['projects'])
    if project_cfg:
        project_id = int(project_cfg['id'])
        try:
            project = gl.projects.get(project_id)
        except GitlabGetError:
            return [discord.Embed(title="Can't handle request",
                                  description="Pls try again later...",
                                  color=discord.Colour.blurple())]
        environments_cfg = project_cfg['envs']
        logging.info(environments_cfg)
        embeds = generate_envs_embeds(project, environments_cfg)
    return embeds
